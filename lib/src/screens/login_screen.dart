import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _LoginScreen();
  }
}
class _LoginScreen extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
        constraints: BoxConstraints.expand(),
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 95,
              ),
              SizedBox(
                width: 245.0,
                child: Image.asset('assets/images/logo.png'),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 40, 0, 6),
                child: SizedBox(
                  width: double.infinity,
                  height: 61.0,
                  child: TextField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            width: 1,
                            color: Color.fromRGBO(0, 0, 0, 0.4),
                            style: BorderStyle.none,
                          ),
                        ),
                        hintText: 'Số điện thoại',
                        hintStyle: TextStyle(
                            color: Color.fromRGBO(110, 103, 103, 1.0)
                        ),
                        prefixIcon: Icon(Icons.phone)
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 6),
                child: SizedBox(
                  width: double.infinity,
                  height: 61.0,
                  child: TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide: BorderSide(
                          width: 1,
                          color: Color.fromRGBO(0, 0, 0, 0.4),
                          style: BorderStyle.none,
                        ),
                      ),
                      hintText: 'Mật khẩu',
                      hintStyle: TextStyle(
                          color: Color.fromRGBO(110, 103, 103, 1.0)
                      ),
                      prefixIcon: Icon(Icons.vpn_key),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 6),
                child: SizedBox(
                  width: double.infinity,
                  height: 61.0,
                  child: RaisedButton(
                    child: Text(
                      'Đăng nhập',
                      style: TextStyle(
                        color: Color.fromRGBO(252, 175, 23, 1),
                        fontSize: 21.0
                      ),
                    ),
                    onPressed: () {},
                    color: Color.fromRGBO(50, 50, 50, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Text(
                  'Quên mật khẩu?',
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    color: Colors.black,
                    fontSize: 21.0
                  ),
                ),
              ),
              SizedBox(
                height: 40.0,
                width: 200.0,
                child: Expanded(
                  child: Divider(
                    height: 1.0,
                    color: Colors.black,
                  ),
                ),
              ),
              Text(
                'Hoặc đăng nhập bằng',
                style: TextStyle(
                  fontSize: 21.0
                ),
              ),
              Center(
                child: Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                        ),
                        RaisedButton(
                          
                          child: new Row(
                            children: <Widget>[
                              new Image.asset(
                                'assets/images/icon-facebook.png',
                                height: 43.0,
                              ),
                            ],
                          ),
                        ),
                        RaisedButton(
                          child: new Row(
                            children: <Widget>[
                              new Image.asset(
                                'assets/images/icon-google.png',
                                height: 43.0,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
